#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>

static struct termios orig_termios;

void disableRaw(void)
{
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios);
}

void enableRawMode(void)
{
    tcgetattr(STDIN_FILENO, &orig_termios);
    atexit(disableRaw);

    struct termios raw = orig_termios;
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    /* disable local-echo, so we don't see everything we press
       and 'canonical' mode so that we get each byte,
       and 'signal' mode so that we don't bail on Ctrl-C,
       and flow-control so Ctrl-S and Ctrl-Q don't stop things,
       and output post-processing (CR->CRLF) */

    tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

int main(int argc, char const *argv[])
{
    (void)argc; (void)argv;

    enableRawMode();

    char c;
    while (read(STDIN_FILENO, &c, 1) == 1 && c != 'q')
    {
        if (iscntrl(c))
        {
            printf("%d\r\n", c);
        }
        else
        {
            printf("%d ('%c')\r\n", c, c);
        }
    }

    return 0;
}
